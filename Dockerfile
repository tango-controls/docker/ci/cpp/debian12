FROM debian:bookworm

ARG APP_UID=2000

ARG APP_GID=2000

LABEL maintainer="TANGO Controls team <contact@tango-controls.org>"

ENV DEBIAN_FRONTEND=noninteractive
ENV CMAKE_PREFIX_PATH=/home/tango/lib/cmake
ENV PKG_CONFIG_PATH=/home/tango/lib/pkgconfig

RUN apt-get update -qq &&                     \
  apt-get install -y --no-install-recommends  \
  apt-utils                                   \
  build-essential                             \
  bzip2                                       \
  ca-certificates                             \
  cmake                                       \
  cppzmq-dev                                  \
  curl                                        \
  docker.io                                   \
  doxygen                                     \
  gdb                                         \
  git                                         \
  graphviz                                    \
  iproute2                                    \
  libc-ares-dev                               \
  libcurl4-openssl-dev                        \
  libjpeg-dev                                 \
  libre2-dev                                  \
  libssl-dev                                  \
  libzmq3-dev                                 \
  libzstd-dev                                 \
  pkg-config                                  \
  pipx                                        \
  python3                                     \
  python3-dev                                 \
  sudo                                        \
  zlib1g-dev                               && \
  rm -rf /var/lib/apt/lists/*

RUN groupadd -g "$APP_GID" tango                               && \
    useradd -l -u "$APP_UID" -g "$APP_GID" -ms /bin/bash tango && \
    usermod -a -G docker,sudo tango                            && \
    echo "%sudo ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers        && \
    echo "/home/tango/lib" > /etc/ld.so.conf.d/home.conf

USER tango

WORKDIR /home/tango

COPY --chown="$APP_UID":"$APP_GID" scripts/*.sh ./

RUN ./common_setup.sh               && \
    ./install_tango_idl.sh          && \
    ./install_omniorb.sh            && \
    ./install_catch.sh              && \
    ./install_opentelemetry_deps.sh && \
    ./install_opentelemetry.sh      && \
    rm -rf dependencies             && \
    pipx install gcovr==6.0

# pipx ensurepath does currently not work for non-login shells,
# see https://github.com/pypa/pipx/issues/1025
ENV PATH=${PATH}:/home/tango/.local/bin
